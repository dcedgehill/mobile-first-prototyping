# Mobile First Prototyping for Goggles.com README
 
The interactive prototype can be accessed at the following links:

* Interactive Prototype on Mobile Devices (https://xd.adobe.com/view/c7bd5a6e-e8ae-49df-9e35-f05ad9a057fe/).
* Interactive Prototype on Desktop Devices (https://xd.adobe.com/view/193e5ce2-f4ce-4ec2-a634-4465b1d05e4b/).